package handlers

import (
	"bytes"
	"context"
	"encoding/json"

	"fmt"
	"log"
	"net/http"

	"github.com/jackc/pgx"
	"gitlab.com/ekename/go-auth.git/model"
)

func (u *UserHandler)  UserLogin(w http.ResponseWriter, r *http.Request) {


	var user model.User

	json.NewDecoder(r.Body).Decode(&user)
	
	ctx := context.Background()


	const sql = "SELECT id, password FROM users WHERE login = $1"

	rows, err := u.DB.Query(ctx, sql, user.Login)

	if err != nil{	
		log.Print("Err select db", err)
	}


	for rows.Next() {

		err = rows.Scan(&userID, &DBPass)

		if err == pgx.ErrNoRows {
			log.Println("Ошибка при получении данных", err)
		} else if err != nil {
			fmt.Println("Данные успешно получены из функции дб")
		}

		salt := string(DBPass[0:8])
		
		var w http.ResponseWriter
		if !bytes.Equal(u.HashPass(user.Password, salt), DBPass) {
			http.Error(w, "Bad pass", http.StatusBadRequest)
		}



	}

	

	CreateSession(w, r, u.DB, userID)
	http.Redirect(w, r, "/courses", http.StatusFound)
}
