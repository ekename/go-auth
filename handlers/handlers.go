package handlers

import (
	"context"
	"database/sql"
	"errors"
	"log"
	"net/http"
	"time"

	"github.com/jackc/pgx/v4/pgxpool"
)

type Session struct {
	UserID uint32
	ID     string
}

// линтер ругается если используем базовые типы в Value контекста
// типа так безопаснее разграничивать
type ctxKey int

const sessionKey ctxKey = 1

var (
	ErrNoAuth = errors.New("No session found")
)

func SessionFromContext(ctx context.Context) (*Session, error) {
	sess, ok := ctx.Value(sessionKey).(*Session)
	if !ok {
		return nil, ErrNoAuth
	}
	return sess, nil
}

func CheckSession(dbpool *pgxpool.Pool, r *http.Request) (*Session, error) {
	ctx := context.Background()
	sessionCookie, err := r.Cookie("session_id")
	if err == http.ErrNoCookie {
		log.Println("CheckSession no cookie")
		return nil, ErrNoAuth
	}

	sess := &Session{}
	row := dbpool.QueryRow(ctx,`SELECT user_id FROM sessions WHERE id = $1`, sessionCookie.Value)
	err = row.Scan(&sess.UserID)
	if err == sql.ErrNoRows {
		log.Println("CheckSession no rows")
		return nil, ErrNoAuth
	} else if err != nil {
		log.Println("CheckSession err:", err)
		return nil, err
	}

	sess.ID = sessionCookie.Value
	return sess, nil
}

func CreateSession(w http.ResponseWriter, r *http.Request, dbpool *pgxpool.Pool, userID uint32) error {
	ctx := context.Background()
	sessID := RandStringRunes(32)
	dbpool.Exec(ctx, "INSERT INTO sessions(id, user_id) VALUES($1, $2)", sessID, userID)

	cookie := &http.Cookie{
		Name:    "session_id",
		Value:   sessID,
		Expires: time.Now().Add(90 * 24 * time.Hour),
		Path:    "/",
	}
	http.SetCookie(w, cookie)
	return nil
}

func DestroySession(w http.ResponseWriter, r *http.Request, dbpool *pgxpool.Pool) error {
	ctx := context.Background()
	sess, err := SessionFromContext(r.Context())
	if err == nil {
		dbpool.Exec(ctx, "DELETE FROM sessions WHERE id = $1", sess.ID)
	}
	cookie := http.Cookie{
		Name:    "session_id",
		Expires: time.Now().AddDate(0, 0, -1),
		Path:    "/",
	}
	http.SetCookie(w, &cookie)
	return nil
}

var (
	noAuthUrls = map[string]struct{}{

		"/user/login": struct{}{},
		"/user/reg":   struct{}{},
		"/":           struct{}{},
	}
)

func AuthMiddleware(dbpool *pgxpool.Pool, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if _, ok := noAuthUrls[r.URL.Path]; ok {
			next.ServeHTTP(w, r)
			return
		}
		sess, err := CheckSession(dbpool, r)
		if err != nil {
			http.Error(w, "No auth", http.StatusUnauthorized)
			return
		}
		ctx := context.WithValue(r.Context(), sessionKey, sess)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}