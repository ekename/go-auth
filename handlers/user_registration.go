package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"net/http"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ekename/go-auth.git/model"

	"golang.org/x/crypto/argon2"
)

type UserHandler struct {
	DB *pgxpool.Pool
}

var (
	DBPass []byte
	userID uint32
	Email  string
)

// Добавляем соль к пароль
// На время иссследований
func (u *UserHandler) HashPass(plainPassword, salt string) []byte {
	hashedPass := argon2.IDKey([]byte(plainPassword), []byte(salt), 1, 64*1024, 4, 32)
	res := []byte(salt)
	return append(res, hashedPass...)
}

// Добавляем соль к пароль
// На время иссследований
func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

// Добавляем соль к пароль
// На время иссследований
var (
	sizes       = []uint{80, 160, 320}
	letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
)

func (u *UserHandler) UserRegistration(w http.ResponseWriter, r *http.Request) {
	ctx := context.Background()
	var user model.User

	json.NewDecoder(r.Body).Decode(&user)


	salt := RandStringRunes(8)

	login := user.Login

	password := user.Password

	pass := u.HashPass(password, salt)

	var id int64

	const sql = "INSERT INTO users (login, password) VALUES($1, $2) RETURNING id"

	err := u.DB.QueryRow(ctx, sql, login, pass).Scan(&id)


	
	if err != nil {
		log.Println("Error when adding a new user to the database PostgreSQL", err)
		http.Error(w, "Error when adding a new user to the database PostgreSQL", http.StatusInternalServerError)
	} else if err == nil {
		fmt.Println("New user good add to Postgres")
		
	} else if err == pgx.ErrNoRows{
		fmt.Println("Err rows")
	}


	CreateSession(w, r, u.DB, uint32(userID))

	http.Redirect(w, r, "/courses/", http.StatusFound)

}
