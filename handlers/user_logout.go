package handlers

import (
	"net/http"
)

func (u *UserHandler) UserLogout(w http.ResponseWriter, r *http.Request) {

	DestroySession(w, r, u.DB)

	http.Redirect(w, r, "/", http.StatusFound)

}
