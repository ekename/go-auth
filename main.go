package main

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/ekename/go-auth.git/handlers"
)

func main() {
	// Соединение с экземпляром Postgres
	ctx := context.Background()
	url := "postgres://postgres:password@localhost:5432/univer?sslmode=disable"

	dbpool, err := pgxpool.Connect(ctx, url)

	if err != nil {
		log.Fatalf("cant connect to db, err: %v\n", err)
	}
	

	u := &handlers.UserHandler{
		DB:   dbpool,
	}

	router := http.NewServeMux()

	fmt.Println("Start server ...")
	router.HandleFunc("/courses/", handlers.Courses)

	router.HandleFunc("/user/reg", u.UserRegistration)
	router.HandleFunc("/user/login", u.UserLogin)
	router.HandleFunc("/user/logout", u.UserLogout)

	router.HandleFunc("/", handlers.Index)
	http.Handle("/", handlers.AuthMiddleware(dbpool, router))

	http.ListenAndServe(":9090", nil)
}
