package model

import "github.com/jackc/pgx/v4/pgxpool"

type User struct {
	Id int64 `json:"id"`

	Login string `json:"login"`

	Password string `json:"password"`
}

type UserHandler struct {
	DB *pgxpool.Pool
}
